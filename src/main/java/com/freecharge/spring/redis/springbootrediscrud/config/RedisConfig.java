package com.freecharge.spring.redis.springbootrediscrud.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
@EnableRedisRepositories
public class RedisConfig {
    /*
    Jedis -> Java Client Library for Redis
     */
    @Bean
    public JedisConnectionFactory connectionFactory(){
        RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
        configuration.setHostName("localhost");
        configuration.setPort(6379);
        return new JedisConnectionFactory(configuration);
    }

    /*
    RedisTemplate is the central class to interact with Redis
    It performs automatic Serialization and Deserialization
    between the given objects and the binary data stored in Redis
    By default RedisTemplate uses Java Serialization
     */
    @Bean
    public RedisTemplate<String, Object> template() {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory());
        /* StringRedisSerializer converts Strings to Bytes[]
         * This will be useful when the interaction with Redis happens through
         * Strings */
        template.setKeySerializer(new StringRedisSerializer());
        /*

         */
        template.setHashKeySerializer(new StringRedisSerializer());
        /*
        JdkSerializationRedisSerializer is to perform serialization and
        deserialization of objects
         */
        template.setHashKeySerializer(new JdkSerializationRedisSerializer());
        template.setValueSerializer(new JdkSerializationRedisSerializer());
        template.setEnableTransactionSupport(true);
        template.afterPropertiesSet();
        return template;
    }
}
