package com.freecharge.spring.redis.springbootrediscrud.controller;

import com.freecharge.spring.redis.springbootrediscrud.entity.Employee;
import com.freecharge.spring.redis.springbootrediscrud.repository.EmployeeRepository;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/employee-service")
public class EmployeeController {

    @Autowired
    EmployeeRepository repository;
    ResponseEntity<?> responseEntity;
    @PostMapping("/employees")
    public ResponseEntity<?> saveEmployee(@RequestBody Employee employee) {
        try {
            responseEntity = new ResponseEntity<>(this.repository.save(employee), HttpStatus.CREATED);
        } catch(Exception e){
            e.printStackTrace();
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @GetMapping("employees")
    public ResponseEntity<?> getEmployeeList() {

        try {
            return ResponseEntity.ok(this.repository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("employees/{id}")
    public ResponseEntity<?> getEmployeeById(@PathVariable("id") int id) {
        ResponseEntity<?> responseEntity = null;
        try {
            responseEntity = new ResponseEntity<>(this.repository.findById(id), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    @PutMapping("employees")
    public ResponseEntity<?> updateEmployee(@RequestBody Employee employee) {
        ResponseEntity<?> responseEntity = null;

        try {
            Employee updatedEmployee = repository.update(employee);
            responseEntity = new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    @DeleteMapping("employees/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable("id") int id) {
        ResponseEntity<?> responseEntity = null;
        try {
            responseEntity = new ResponseEntity<>(this.repository.delete(id), HttpStatus.OK);
        } catch (Exception e) {
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!!", HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return responseEntity;
    }


}
